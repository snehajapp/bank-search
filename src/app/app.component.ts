import { Component } from '@angular/core';
import { SelectItem } from 'primeng/components/common/selectitem';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CountryServiceService } from './services/country-service.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  [x: string]: any;
  title = 'BankSearch';
  cities1: SelectItem[];

  country: any;

  countries: any[];

  filteredCountriesSingle: any[];

  readonly ROOT_URL = 'https://vast-shore-74260.herokuapp.com/banks?city=';

  posts: Observable<any>;

  bankData = [];

  constructor(private http: HttpClient, private countryService: CountryServiceService) {
    this.cities1 = [
      { label: 'Select City', value: null },
      { label: 'MUMBAI', value: { id: 1, name: 'MUMBAI', code: 'NY' } },
      { label: 'PUNE', value: { id: 2, name: 'PUNE', code: 'RM' } },
      { label: 'CHENNAI', value: { id: 3, name: 'CHENNAI', code: 'LDN' } },
      { label: 'DELHI', value: { id: 4, name: 'DELHI', code: 'IST' } },
      { label: 'BANGALORE', value: { id: 5, name: 'BANGALORE', code: 'PRS' } }
    ];
  }

  getPosts(location) {
    console.log(location);
    console.log(this.ROOT_URL + location);
    this.posts = this.http.get(this.ROOT_URL + location);

    this.posts.subscribe((res) => {
      this.bankData = [];
      for (let i = 0; i < res.length; i++) {
        let obj = { "name": String(res[i].bank_name), "code": String(res[i].ifsc) }
        this.bankData.push(obj);
      }

      console.log(this.bankData);
    })

  }

  onSelectionofDropDown(event) {
    console.log(event.value.name);
    this.getPosts(event.value.name);
  }

  text: string;

  results: string[];

  search(event) {
    this.mylookupservice.getResults(event.query).then(data => {
      this.results = data;
    });
  }

  filterCountrySingle(event) {
    console.log(event, event.query);
    let query = event.query;

    // this.countryService.getCountries().subscribe((countries:any)=>{
    //   console.log(countries.data);
    //   this.filteredCountriesSingle = this.filterCountry(query, countries.data);
    // });
    console.log(this.bankData);
    this.filteredCountriesSingle = this.filterCountry(query, this.bankData);
  }

  filterCountry(query, countries: any[]): any[] {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: any[] = [];
    for (let i = 0; i < countries.length; i++) {
      let country = countries[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }
    return filtered;
  }
}
